package com.hendrix.test.springbootdocker.service;

public interface AddressService {
	public String getServerAddress() throws Exception;
}
