package com.hendrix.test.springbootdocker.service.impl;

import java.net.InetAddress;

import org.springframework.stereotype.Service;

import com.hendrix.test.springbootdocker.service.AddressService;

@Service
public class AddressServiceImpl implements AddressService {

	public String getServerAddress() throws Exception {
		final String serverAddress = InetAddress.getLocalHost().getHostAddress();
		return serverAddress;
	}
}
