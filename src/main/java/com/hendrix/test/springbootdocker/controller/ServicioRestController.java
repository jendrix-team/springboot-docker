package com.hendrix.test.springbootdocker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hendrix.test.springbootdocker.service.AddressService;

@RestController
public class ServicioRestController {
	private final AddressService service;

	@Autowired
	public ServicioRestController(AddressService service) {
		this.service = service;
	}

	@RequestMapping(value = "/microservice")
	public String hello() throws Exception {

		String serverAddress = service.getServerAddress();
		return new StringBuilder().append("Hello from IP address: ").append(serverAddress).toString();
	}

}
